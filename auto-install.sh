#!/bin/bash

# Check dir before cloning
check_dir() {
	if [ -d "dumbplay" ]; then
		echo "Directory Dumbplay already exist, skipping...."
	else
		echo "Cloning Dumbplay...."
		echo -n "Enter valid git URL : "
		read giturl
		git clone $giturl
	fi
}

# Check file before download
check_file() {
	if [ -f "mysql-apt-config_0.8.15-1_all.deb" ]; then
		echo "mysql-apt-config_0.8.15-1_all.deb already exist, skipping...."
	else
		echo "Downloading mysql-apt-config_0.8.15-1_all.deb...."
		wget https://dev.mysql.com/get/mysql-apt-config_0.8.15-1_all.deb
	fi
}

# Node.Js Clan
# Install Node.Js Dependencies
node_install() {
	curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
	sudo apt upgrade -y
	sudo apt-get install -y nodejs
	echo "Install PM2"
	sudo npm install -g pm2
	# Clone repo
	check_dir
}
node_install2() {
	echo "npm install, wait a sec...."
	npm install
	pm2 start ecosystem.config.js
}

# Menu list
menu_list() {
	echo "1. Database"
	echo "2. Backend"
	echo "3. Frontend"
	echo "4. SSL Installation"
	echo "5. Reverse Proxy"
	echo "6. First time? Config user?"
	echo "Make sure to install database first before backend"
	echo -n "Pick one to install [1/2/3/4/5/6] : "
}

# Debian based update
sudo apt update

answer=7
menu_list

# Loop until gets the job done!
while [ $answer -eq 7 ]; do
	read answer

	if [ $answer -eq 1 ]; then
		echo "Database Installation"
		check_file
		sudo dpkg -i mysql-apt-config_0.8.15-1_all.deb
		sudo apt update && sudo apt upgrade -y
		sudo apt install mysql-server
		echo "Please, Login to MySQL Console!"
		echo "Then create database called : dumbplay"
		mysql -u root -p
		sudo systemctl status mysql
	elif [ $answer -eq 2 ]; then
		node_install
		echo "Going to Backend Folder..."
		echo "Install sequelize-cli"
		sudo npm i -g sequelize-cli
		# Make sure you're in home dir
		cd
		cd dumbplay-backend
		cp .env-copy .env
		nano config/config.json
		sequelize db:migrate
		node_install2
	elif [ $answer -eq 3 ]; then
		node_install
		echo "Going to Frontend Folder..."
		echo "Almost Done"
		# Make sure you're in home dir
		cd
		cd dumplay-frontend
		nano src/config/api.js
		node_install2
	elif [ $answer -eq 4 ]; then
		echo "Installing certbot"
		sudo snap install core
		sudo snap refresh core
		sudo snap install --classic certbot
		sudo ln -s /snap/bin/certbot /usr/bin/certbot
		echo -n "Enter your domain : "
		read domain
		sudo certbot --nginx -d $domain
	elif [ $answer -eq 5 ]; then
		sudo apt install nginx -y
		cd /etc/nginx
		echo -n "Enter your desire name : "
		read proxy
		sudo mkdir $proxy
		wget https://gitlab.com/ledleledle/auto-install-app/-/raw/master/conftemp
		sudo mv conftemp $proxy/$proxy.conf
		sudo nano nginx.conf
		sudo nano $proxy/$proxy.conf
		cd
	elif [ $answer -eq 6 ]; then
		echo "Add user"
		echo -n "Enter your username : "
		read username
		sudo adduser $username
		sudo usermod -aG sudo $username
		sudo nano /etc/ssh/sshd_config
		echo "Restarting sshd.service...."
		sudo systemctl restart sshd.service
	else
		menu_list
		answer=7
	fi
done

echo "=================================="
echo "########## Script Ended ##########"
echo "=================================="
